const jwt = require("jsonwebtoken");
import getConfig from "next/config";

import { apiHandler } from "helpers/api";

const { serverRuntimeConfig } = getConfig();

// users in JSON file for simplicity, store in a db for production applications
// const users = require("data/users.json");

//hieu test
import { MongoClient } from "mongodb";
let uri =
  "mongodb://127.0.0.1:27017/?readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false";
let dbName = "socialdb";

let cachedClient = null;
let cachedDb = null;

if (!uri) {
  throw new Error(
    "Please define the MONGODB_URI environment variable inside .env.local"
  );
}

if (!dbName) {
  throw new Error(
    "Please define the MONGODB_DB environment variable inside .env.local"
  );
}

async function connectToDatabase() {
  if (cachedClient && cachedDb) {
    return { client: cachedClient, db: cachedDb };
  }

  const client = await MongoClient.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  const db = await client.db(dbName);

  cachedClient = client;
  cachedDb = db;

  return { client, db };
}
//hieu test
export default apiHandler(handler);

function handler(req, res) {
  switch (req.method) {
    case "POST":
      return authenticate();
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`);
  }

  async function getUser(){
    const { db } = await connectToDatabase();
    const users = await db
    .collection("user")
    .find({})
    .toArray();
    console.log(users);
    return users;
  }
  async function authenticate() {
    const { username, password } = req.body;

    const { db } = await connectToDatabase();
    const userList = await db
    .collection("user")
    .find( {}).toArray();

    const user = userList.find(
      (u) => u.phone === username && u.password === password
    );

    if (!user) throw "Username or password is incorrect";

    // create a jwt token that is valid for 7 days
    const accessToken = jwt.sign(
      { sub: user.id, access: true },
      serverRuntimeConfig.secret,
      { expiresIn: "7d" }
    );
    const refreshToken = jwt.sign(
      { sub: user.id, refresh: true },
      serverRuntimeConfig.secret,
      { expiresIn: "30d" }
    );

    // return basic user details and token
    return res.status(200).json({
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      accessToken,
      refreshToken,
    });
  }
}
